# Using Traefik as a Reverse Proxy for Docker Containers
The information was taken from here: [Using Traefik as a Reverse Proxy for Docker Containers](https://www.digitalocean.com/community/tutorials/how-to-use-traefik-as-a-reverse-proxy-for-docker-containers-on-ubuntu-18-04-ru)
## Step 1 — Configuring and Running Traefik
Clone this repository.  
The Traefik project has an [official Docker image](https://hub.docker.com/_/traefik), so we will use that to run Traefik in a Docker container.  
But before we get our Traefik container up and running, we need to create a configuration file and set up an encrypted password so we can access the monitoring dashboard.  
We’ll use the *htpasswd* utility to create this encrypted password. First, install the utility, which is included in the *apache2-utils* package:
```
sudo apt-get install apache2-utils
```
Then generate the password with *htpasswd*. Substitute *secure_password* with the password you’d like to use for the Traefik admin user:
```
htpasswd -nb admin secure_password
```
The output from the program will look like this:
```
Output
admin:$apr1$ruca84Hq$mbjdMZBAG.KWn7vfN/SNK/
```
You’ll use this output in the Traefik configuration file to set up HTTP Basic Authentication for the Traefik health check and monitoring dashboard. Copy the entire output line so you can paste it later.  

To configure the Traefik server, we’ll need to edit a configuration file called *traefik.toml* using the TOML format. TOML is a configuration language similar to INI files, but standardized. This file lets us configure the Traefik server and various integrations, or providers, we want to use. In this tutorial, we will use three of Traefik’s available providers: *api, docker,* and *acme,* which is used to support TLS using Let’s Encrypt.  

So, edit *traefik.toml* to configure the *api* provider, which gives you access to a dashboard interface. This is where you’ll paste the output from the *htpasswd* command.

```
...
[entryPoints]
  [entryPoints.dashboard]
    address = ":8080"
    [entryPoints.dashboard.auth]
      [entryPoints.dashboard.auth.basic]
        users = ["admin:*your_encrypted_password*"]

[api]
entrypoint="dashboard"
...
```

The dashboard is a separate web application that will run within the Traefik container. We set the dashboard to run on port *8080*.  

The *entrypoints.dashboard* section configures how we’ll be connecting with with the *api* provider, and the *entrypoints.dashboard.auth.basic* section configures HTTP Basic Authentication for the dashboard. Use the output from the *htpasswd* command you just ran for the value of the *users* entry. You could specify additional logins by separating them with commas.

Next, edit *email* section and add to it *your email*. This section configure a Let’s Encrypt certificate support for Traefik:

```
...
[acme]
email = "*your_email@your_domain*"
storage = "acme.json"
entryPoint = "https"
onHostRule = true
  [acme.httpChallenge]
  entryPoint = "http"
...
```
This section is called *acme* because ACME is the name of the protocol used to communicate with Let’s Encrypt to manage certificates. The Let’s Encrypt service requires registration with a valid email address, so in order to have Traefik generate certificates for our hosts, set the *email* key to your email address. We then specify that we will store the information that we will receive from Let’s Encrypt in a JSON file called *acme.json*. The *entryPoint* key needs to point to the entry point handling port *443*, which in our case is the *https* entry point.

The key *onHostRule* dictates how Traefik should go about generating certificates. We want to fetch our certificates as soon as our containers with specified hostnames are created, and that’s what the *onHostRule* setting will do.

The *acme.httpChallenge* section allows us to specify how Let’s Encrypt can verify that the certificate should be generated. We’re configuring it to serve a file as part of the challenge through the *http* entrypoint.

Finally, let’s configure the docker provider by editing *domain*, replace *your_domain* with your domain name:

```
...
[docker]
domain = "your_domain"
watch = true
network = "web"
```

The *docker* provider enables Traefik to act as a proxy in front of Docker containers. We’ve configured the provider to *watch* for new containers on the *web* network (that we’ll create soon) and expose them as subdomains of *your_domain*.

## Step 2 — Running the Traefik Container
Next, create a Docker network for the proxy to share with containers. The Docker network is necessary so that we can use it with applications that are run using Docker Compose. Let’s call this network *web*.
```
docker network create web
```
When the Traefik container starts, we will add it to this network. Then we can add additional containers to this network later for Traefik to proxy to.

Finally, create the Traefik container with this command:
```
docker-compose up -d
```
With the container started, you now have a dashboard you can access to see the health of your containers. You can also use this dashboard to visualize the frontends and backends that Traefik has registered. Access the monitoring dashboard by pointing your browser to *https://monitor.your_domain*. You will be prompted for your username and password, which are admin and the password you configured in Step 1.
## Step 3 — Registering Containers with Traefik, Seting up a new container
For examle:  
You need to create new folder and name it with your container. Let's call it apache.
```
mkdir apache
```
In this folder create file called docker-compose.yml
```
nano docker-compose.yml
```
Listing of file:
```
version: "3"

networks:
  web:
    external: true
  internal:
    external: false

services:

  apache:
    image: httpd:2.4
    volumes:
      - ./www:/usr/local/apache2/htdocs/
    labels:
      - traefik.backend=apache
      - traefik.frontend.rule=Host:web.your_domain
      - traefik.docker.network=web
      - traefik.port=80
    networks:
      - internal
      - web
```
In this file, we will attach the volume to our container, it will be located in the *"www"* folder, which we will also need to create, and place the "index.html" file there.
```
    volumes:
      - ./www:/usr/local/apache2/htdocs/
```
We also need to name or change the name of the service, in our case *"apache"*  
Specify the docker image we will be using - *"image: httpd:2.4"*  
*"traefik.backend"* - sets the name of the backend service in Traefik (which points to the actual *"apache"* container which must match the service name)  
Go to the folder with the future container and run the containers using *docker-compose*:
```
docker-compose up -d
```
Now take another look at the Traefik admin dashboard. You’ll see that there is now a backend and a frontend for the two exposed servers.  
Navigate to *web.your_domain*, substituting *your_domain* with your domain. You’ll be redirected to a TLS connection and can now see your *index.html* page.